function manifest()
    myManifest = {
        name          = "Vocaloid Intonation Tool",
        comment       = "Use PIT to set a different intonation.",
        author        = "kirakiratsundere and kÃ¡vÃ©zÃ³",
        pluginID      = "{F2769fD9-F397-C160-4D04-61F219E05521}", --idk how to get this. Right now, this is just a random hex sequence :I
        pluginVersion = "0.1.0.0",
        apiVersion    = "4.0.0.1"
    }

    return myManifest
end

--entry point
--a.k.a. THE GOD FUNCTION (lol I promise to clean it up sometime >.<)
function main(processParam, envParam)
	local beginPosTick = processParam.beginPosTick -- for some reason all job plugins have this stuff. It's not used later.
	local endPosTick   = processParam.endPosTick
	local songPosTick  = processParam.songPosTick

	local scriptDir  = envParam.scriptDir
	local scriptName = envParam.scriptName
	local tempDir    = envParam.tempDir

  --get pitch controls. To add with later.
  local pitControl = {}
  local pitControlList = {}

  --get pbs controls. To multiply with later.
  local pbsControl = {}
  local pbsControlList = {}

  local retCode --"return code". Dummy variable for absorbing error codes.
	local idx --"index"

  retCode = VSSeekToBeginControl("PIT")
	idx = 1
	retCode, pitControl = VSGetNextControl("PIT")
	while (retCode == 1) do
		pitControlList[idx] = pitControl
		retCode, pitControl = VSGetNextControl("PIT")
		idx = idx + 1
	end
  if #pitControlList==0 then
    pitControlList[idx] = {posTick=0, value=0, type="PIT"}
  end

  retCode = VSSeekToBeginControl("PBS")
	idx = 1
	retCode, pbsControl = VSGetNextControl("PBS")
	while (retCode == 1) do
		pbsControlList[idx] = pbsControl
		retCode, pbsControl = VSGetNextControl("PBS")
		idx = idx + 1
	end
  if #pbsControlList==0 then
    pbsControlList[idx] = {posTick=0, value=2, type="PBS"}
  end

  --get user options - pitch center, type of intonation--

  notes = invertTable1({"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"}) --like enumerating the notes

	VSDlgSetDialogTitle("Vocaloid4 Intonation Tool") 						--dialog
	local centerField = {} 								--make drop-down menu
	centerField.name       = "center"
	centerField.caption    = "Center:"
	centerField.initialVal = "C, C#, D, D#, E, F, F#, G, G#, A, A#, B"
	centerField.type = 4
	dlgStatus  = VSDlgAddField(centerField)

  local tuningField = {} 								--make drop-down menu
	tuningField.name       = "tuning"
	tuningField.caption    = "Tuning:"
	tuningField.initialVal = "Pythagorean, Just intonation, Quarter-comma Meantone"
	tuningField.type = 4
	dlgStatus  = VSDlgAddField(tuningField)

  local tritoneField = {} 								--make drop-down menu
	tritoneField.name       = "tritone"
	tritoneField.caption    = "Tritone:"
	tritoneField.initialVal = "Augmented Fourth, Tritone (equal temperament), Diminished Fifth"
	tritoneField.type = 4
	dlgStatus  = VSDlgAddField(tritoneField)
	dlgStatus = VSDlgDoModal()
	-- cancel
	if (dlgStatus == 2) then
		return 0
	end

	-- error on weird return codes
	if ((dlgStatus ~= 1) and (dlgStatus ~= 2)) then
		return 1
	end

  dlgStatus, center = VSDlgGetStringValue("center")
  centerValue = notes[center]

  dlgStatus, tuning = VSDlgGetStringValue("tuning")
  dlgStatus, tritone = VSDlgGetStringValue("tritone")

  --get notes--

  noteList = {}

  VSSeekToBeginNote()
	idx = 1
	retCode, note = VSGetNextNote()
	while (retCode == 1) do
		noteList[idx] = note
		retCode, note = VSGetNextNote()
		idx = idx + 1
	end
  if (#noteList == 0) then
    return 0
  end

  --make tuning lists--

  pythPITs = {0, -801, 320, -480, 641, -160, 0, 160, -641, 480, -320, 801}
  justPITs = {0, 961, 320, 1281, -1121, -160, 0, 160, 1121, -1281, -320, -961}
  qcmiPITs = {0, 1401, -561, 841, -1121, 280, 0, -280, 1121, 841, 561, -1401}

  if tritone=="Augmented Fourth" then
    pythPITs[7]=961
    justPITs[7]=-801
    qcmiPITs[7]=-1682
  elseif tritone=="Diminished Fifth" then
    pythPITs[7]=-961
    justPITs[7]=801
    qcmiPITs[7]=1682
  else
    pythPITs[7]=0
    justPITs[7]=0
    qcmiPITs[7]=0
  end

  local tuneList = {}

  if      tuning=="Pythagorean"             then tuneList=pythPITs
  elseif  tuning=="Just intonation"         then tuneList=justPITs
  elseif  tuning=="Quarter-comma Meantone"  then tuneList=qcmiPITs
  else                                           tuneList = {0,0,0,0,0,0,0,0,0,0,0,0}
  end --pretty-printed this on a whim ._.

  --merge PIT, PBS, and actual note pitches into one table. Essentially just the merge step from a mergesort based on tick.--

  allControlList = {}

  pitIdx = 1
  pbsIdx = 1
  noteIdx = 1

  npitDone = (pitIdx < #pitControlList+1) --if there are still things in each list
  npbsDone = (pbsIdx < #pbsControlList+1)
  nnoteDone = (noteIdx < #noteList+1)

  while (npitDone or npbsDone or nnoteDone)
  do
    local currTickList = {}
    if npitDone then
      currTickList[1] = pitControlList[pitIdx].posTick
    end
    if npbsDone then
      currTickList[2] = pbsControlList[pbsIdx].posTick
    end
    if nnoteDone then
      currTickList[3] = noteList[noteIdx].posTick
    end

    local minIdx = firstMin(currTickList)

    if minIdx==1 and npitDone then
      table.insert(allControlList, {type=1, value=pitControlList[pitIdx].value, tick=pitControlList[pitIdx].posTick})
      pitIdx=pitIdx+1
    elseif minIdx==2 and npbsDone then
      table.insert(allControlList, {type=2, value=pbsControlList[pbsIdx].value, tick=pbsControlList[pbsIdx].posTick})
      pbsIdx=pbsIdx+1
    elseif minIdx==3 and nnoteDone then
      table.insert(allControlList, {type=3, value=noteList[noteIdx].noteNum, tick=noteList[noteIdx].posTick})
      noteIdx=noteIdx+1
    else return 1
    end

    npitDone = (pitIdx < #pitControlList+1)
    npbsDone = (pbsIdx < #pbsControlList+1)
    nnoteDone = (noteIdx < #noteList+1)
  end

  --get final pitches, set pitches--

  currPIT = 0
  currPBS = 2
  currNote = 59 + notes[center] --makes the first PIT point 0.

  for idx = 1, #pitControlList do
		retCode = VSRemoveControl(pitControlList[idx])
	end

  for index, point in pairs(allControlList) do
    if point.type==1 then currPIT=point.value
    elseif point.type==2 then currPBS=point.value
    elseif point.type==3 then currNote=point.value
    else return 1
    end
    newpitch = (tuneList[(currNote-notes[center]+2)%12]*(1/currPBS))+currPIT
    retcode = VSUpdateControlAt("PIT", point.tick, newpitch)
  end
end

function invertTable1(table)
  local returnme = {}
  for index, val in pairs(table) do
    returnme[val] = index
  end
  return returnme
end

function firstMin(table)
  local minIdx = 1
  local minVal = math.huge
  for index, val in pairs(table) do
    if(val<minVal) then
      minIdx = index
      minVal = val
    end
  end
  return minIdx
end

--[[
function printRecord(table)
  string = ""
  for key, val in pairs(table) do
    string = string..key..": "..tostring(val)..", "
  end
  return string
end
--]]
